import junit.framework.TestCase;

/**
 * Test cases for Fibonacci class.
 * @author Karol Topolski
 */
public class FibonacciTest extends TestCase {
    private static Fibonacci fib = null;
    
    public FibonacciTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        fib = new Fibonacci();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testForbidNegativeNumbers() {
        try {
            fib.next(-1);
        } catch(IllegalArgumentException iae) {
            assertEquals(iae.getMessage(), "negative number is forbidden");
        }
    }

    public void testRecurrencyStopCondition() {
        assertEquals(fib.next(0), 1);
        assertEquals(fib.next(1), 1);
    }

    public void testFibonacciSequence() {
        int[][] sequence = new int[][] {
            {0, 1},
            {1, 1},
            {2, 2},
            {3, 3},
            {4, 5},
            {5, 8},
            {6, 13},
            {7, 21}
        };
        for(int i = 0; i < sequence.length; i++)
            assertEquals(fib.next(sequence[i][0]), sequence[i][1]);
    }
}
