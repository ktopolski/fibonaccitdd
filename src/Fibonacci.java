/**
 * Simple Fibonacci series calculator.
 * @author Karol Topolski
 */
public class Fibonacci {
    /**
     * Calculates next fibonacci series from given number.
     * @param number number to calculate fibonacci series from
     * @return fibonacci series number after given number
     * @throws IllegalArgumentException when negative number is given
     */
    public long next(int number) throws IllegalArgumentException {
        if(number < 0)
            throw new IllegalArgumentException("negative number is forbidden");
        else if(number < 2) return 1;
        else return next(number - 1) + next(number - 2);
    }
}
